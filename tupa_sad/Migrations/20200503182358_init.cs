﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tupa_sad.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "client",
                columns: table => new
                {
                    client_id = table.Column<string>(maxLength: 36, nullable: false),
                    first_name = table.Column<string>(maxLength: 255, nullable: false),
                    last_name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_client", x => x.client_id);
                });

            migrationBuilder.CreateTable(
                name: "employer",
                columns: table => new
                {
                    employer_id = table.Column<string>(maxLength: 36, nullable: false),
                    first_name = table.Column<string>(maxLength: 255, nullable: false),
                    last_name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employer", x => x.employer_id);
                });

            migrationBuilder.CreateTable(
                name: "equipment",
                columns: table => new
                {
                    equipment_id = table.Column<string>(maxLength: 36, nullable: false),
                    name = table.Column<string>(maxLength: 255, nullable: false),
                    description = table.Column<string>(maxLength: 255, nullable: true),
                    is_company_ownership = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_equipment", x => x.equipment_id);
                });

            migrationBuilder.CreateTable(
                name: "service",
                columns: table => new
                {
                    service_id = table.Column<string>(maxLength: 36, nullable: false),
                    name = table.Column<string>(maxLength: 255, nullable: false),
                    description = table.Column<string>(maxLength: 255, nullable: true),
                    price = table.Column<decimal>(nullable: false),
                    is_avaible = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_service", x => x.service_id);
                });

            migrationBuilder.CreateTable(
                name: "request",
                columns: table => new
                {
                    request_id = table.Column<string>(maxLength: 36, nullable: false),
                    service_id = table.Column<string>(maxLength: 36, nullable: true),
                    client_id = table.Column<string>(maxLength: 36, nullable: true),
                    employer_id = table.Column<string>(maxLength: 36, nullable: true),
                    equipment_id = table.Column<string>(maxLength: 36, nullable: true),
                    create_at = table.Column<DateTime>(nullable: false, defaultValueSql: "now()"),
                    close_at = table.Column<DateTime>(nullable: true),
                    is_completed = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("request_pkey", x => x.request_id);
                    table.ForeignKey(
                        name: "request_client_id_fkey",
                        column: x => x.client_id,
                        principalTable: "client",
                        principalColumn: "client_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "request_employer_id_fkey",
                        column: x => x.employer_id,
                        principalTable: "employer",
                        principalColumn: "employer_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "request_equipment_id_fkey",
                        column: x => x.equipment_id,
                        principalTable: "equipment",
                        principalColumn: "equipment_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "request_service_id_fkey",
                        column: x => x.service_id,
                        principalTable: "service",
                        principalColumn: "service_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_request_client_id",
                table: "request",
                column: "client_id");

            migrationBuilder.CreateIndex(
                name: "IX_request_employer_id",
                table: "request",
                column: "employer_id");

            migrationBuilder.CreateIndex(
                name: "IX_request_equipment_id",
                table: "request",
                column: "equipment_id");

            migrationBuilder.CreateIndex(
                name: "IX_request_service_id",
                table: "request",
                column: "service_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "request");

            migrationBuilder.DropTable(
                name: "client");

            migrationBuilder.DropTable(
                name: "employer");

            migrationBuilder.DropTable(
                name: "equipment");

            migrationBuilder.DropTable(
                name: "service");
        }
    }
}
