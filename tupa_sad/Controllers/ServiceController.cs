﻿using Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Requests.Abstractions;
using Requests.Abstractions.Models;
using Requests.Abstractions.Requests;
using Requests.Abstractions.Responses;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace tupa_sad.Controllers
{
    [ApiController]
    [Route("api/services")]
    public class ServiceController : ControllerBase
    {
        private readonly IServiceService serviceService;

        public ServiceController(IServiceService serviceService)
        {
            this.serviceService = serviceService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PagingResponse<ServiceResponse>), StatusCodes.Status200OK)]
        public async Task<ActionResult<PagingResponse<ServiceResponse>>> GetServices(int page = 1, int perPage = 100)
        {
            var services = await serviceService.GetAllAsResponses(page, perPage);
            var response = new PagingResponse<ServiceResponse>()
            {
                Data = services.ToArray(),
                Page = page,
                PerPage = perPage,
                Pages = await serviceService.Pages(perPage, x => x.IsAvaible)
            };
            return response;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ServiceResponse>> GetService(string id)
        {
            Service service = await serviceService.Get(id);
            return new ServiceResponse(service);
        }

        [HttpPost]
        public async Task<ActionResult<ServiceResponse>> CreateService([FromBody]CreateServiceRequest request)
        {
            var service = await serviceService.Create(new Service
            {
                ServiceId = Guid.NewGuid().ToString(),
                Name = request.Name,
                Description = request.Description,
                Price = request.Price,
                IsAvaible = request.IsAvaible
            });
            return new ServiceResponse(service);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteService(string id)
        {
            await serviceService.Delete(id);
            
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<ServiceResponse>> UpdateService(string id, UpdateServiceRequest updateServiceRequest)
        {
            if (id != updateServiceRequest.ServiceId)
                return BadRequest();

            Service service = await serviceService.Update(updateServiceRequest);

            return new ServiceResponse(service);
        }
    }
}
