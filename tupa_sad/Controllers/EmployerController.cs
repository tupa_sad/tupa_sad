﻿using Employers.Abstractions;
using Employers.Abstractions.Models;
using Employers.Abstractions.Requests;
using Employers.Abstractions.Responses;
using Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tupa_sad.Models;
using tupa_sad.Requests;
using tupa_sad.Responses;
using tupa_sad.Services;

namespace tupa_sad.Controllers
{
    [ApiController]
    [Route("api/employers")]
    public class EmployerController : ControllerBase
    {
        private readonly IEmployerService employerService;

        public EmployerController(IEmployerService employerService)
        {
            this.employerService = employerService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PagingResponse<EmployerResponse>), StatusCodes.Status200OK)]
        public async Task<ActionResult<PagingResponse<EmployerResponse>>> GetEmployers(int page = 1, int perPage = 100)
        {
            var employers = await employerService.GetAllAsResponses(page, perPage);
            var response = new PagingResponse<EmployerResponse>()
            {
                Data = employers.ToArray(),
                Page = page,
                PerPage = perPage,
                Pages = await employerService.Pages(perPage)
            };
            return response;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<EmployerResponse>> GetEmployer(string id)
        {
            Employer employer = await employerService.Get(id);
            return new EmployerResponse(employer);
        }

        [HttpPost]
        public async Task<ActionResult<EmployerResponse>> CreateEmployer([FromBody]CreateEmployerRequest request)
        {
            var employer = await employerService.Create(new Employer
            {
                EmployerId = Guid.NewGuid().ToString(),
                FirstName = request.FirstName,
                LastName = request.LastName
            });
            return new EmployerResponse(employer);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployer(string id)
        {
            await employerService.Delete(id);
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<EmployerResponse>> UpdateEmployer(string id, UpdateEmployerRequest updateEmployerRequest)
        {
            if (id != updateEmployerRequest.EmployerId)
            {
                return BadRequest();
            }
            Employer employer = await employerService.Update(updateEmployerRequest);
            return new EmployerResponse(employer);
        }
    }
}
