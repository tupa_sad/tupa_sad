﻿using Equipments.Abstractions;
using Equipments.Abstractions.Models;
using Equipments.Abstractions.Requests;
using Equipments.Abstractions.Responses;
using Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tupa_sad.Controllers
{
    [ApiController]
    [Route("api/equipments")]
    public class EquipmentController : ControllerBase
    {
        private readonly IEquipmentService equipmentService;

        public EquipmentController(IEquipmentService equipmentService)
        {
            this.equipmentService = equipmentService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PagingResponse<EquipmentResponse>), StatusCodes.Status200OK)]
        public async Task<ActionResult<PagingResponse<EquipmentResponse>>> GetEquipments(int page = 1, int perPage = 100)
        {
            var equipments = await equipmentService.GetAllAsResponses(page, perPage);
            var response = new PagingResponse<EquipmentResponse>()
            {
                Data = equipments.ToArray(),
                Page = page,
                PerPage = perPage,
                Pages = await equipmentService.Pages(perPage)
            };
            return response;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<EquipmentResponse>> GetEquipment(string id)
        {
            Equipment equipment = await equipmentService.Get(id);
            return new EquipmentResponse(equipment);
        }

        [HttpPost]
        public async Task<ActionResult<EquipmentResponse>> CreateEquipment([FromBody]CreateEquipmentRequest request)
        {
            var service = await equipmentService.Create(new Equipment
            {
                EquipmentId = Guid.NewGuid().ToString(),
                Name = request.Name,
                Description = request.Description
            });
            return new EquipmentResponse(service);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEquipment(string id)
        {
            await equipmentService.Delete(id);
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<EquipmentResponse>> UpdateEquipment(string id, UpdateEquipmentRequest updateEquipmentRequest)
        {
            if (id != updateEquipmentRequest.EquipmentId)
            {
                return BadRequest();
            }
            Equipment equipment = await equipmentService.Update(updateEquipmentRequest);
            return new EquipmentResponse(equipment);
        }
    }
}
