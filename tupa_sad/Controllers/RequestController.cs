﻿using Employers.Abstractions;
using Employers.Abstractions.Models;
using Equipments.Abstractions;
using Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Requests.Abstractions;
using Requests.Abstractions.Models;
using Requests.Abstractions.Requests;
using Requests.Abstractions.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tupa_sad.Controllers
{
    [ApiController]
    [Route("api/requests")]
    public class RequestController : ControllerBase
    {
        private readonly IRequestService requestService;
        private readonly IEmployerService employerService;
        private readonly IEquipmentService equipmentService;
        private readonly IPatientService patientService;

        public RequestController(IRequestService requestService,
            IEmployerService employerService,
            IEquipmentService equipmentService,
            IPatientService patientService)
        {
            this.requestService = requestService;
            this.employerService = employerService;
            this.equipmentService = equipmentService;
            this.patientService = patientService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PagingResponse<RequestResponse>), StatusCodes.Status200OK)]
        public async Task<ActionResult<PagingResponse<RequestResponse>>> GetRequests(int page = 1, int perPage = 100, )
        {
            var requests = await requestService.GetAllAsResponses(page, perPage);
            var response = new PagingResponse<RequestResponse>()
            {
                Data = requests.ToArray(),
                Page = page,
                PerPage = perPage,
                Pages = await requestService.Pages(perPage, x => !x.IsCompleted)
            };
            return response;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<RequestResponse>> GetRequest(string id)
        {
            Request request = await requestService.Get(id);
            return new RequestResponse(request);
        }

        [HttpPost]
        public async Task<ActionResult<RequestResponse>> CreateRequest([FromBody]CreateRequestRequest request)
        {
            var service = await requestService.Create(new Request
            {
                RequestId = Guid.NewGuid().ToString(),
                ServiceId = request.ServiceId,
                ClientId = request.ClientId,
                CreateAt = DateTime.Now,
                CloseAt = null,
                IsCompleted = false,
                Employers = await Task.WhenAll(request.Employers.Select(async e => await employerService.Get(e)).ToList()),
                Equipments = await Task.WhenAll(request.Equipments.Select(async e => await equipmentService.Get(e)).ToList()),
                Patients = await Task.WhenAll(request.Patients.Select(async e => await patientService.Get(e)).ToList())
            });
            return new RequestResponse(service);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRequest(string id)
        {
            await requestService.Delete(id);
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<RequestResponse>> UpdateRequest(string id, UpdateRequestRequest updateRequestRequest)
        {
            if (id != updateRequestRequest.RequestId)
            {
                return BadRequest();
            }

            Request request = await requestService.Update(updateRequestRequest);
            return new RequestResponse(request);
        }
    }
}
