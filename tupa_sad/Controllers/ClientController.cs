﻿using Clients.Abstractions;
using Clients.Abstractions.Models;
using Clients.Abstractions.Requests;
using Clients.Abstractions.Responses;
using Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tupa_sad.Controllers
{
    [ApiController]
    [Route("api/clients")]
    public class ClientController : ControllerBase
    {
        private readonly IClientService clientService;

        public ClientController(IClientService clientService)
        {
            this.clientService = clientService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PagingResponse<ClientResponse>), StatusCodes.Status200OK)]
        public async Task<ActionResult<PagingResponse<ClientResponse>>> GetClients(int page = 1, int perPage = 100)
        {
            var clients = await clientService.GetAllAsResponses(page, perPage);
            var response = new PagingResponse<ClientResponse>()
            {
                Data = clients.ToArray(),
                Page = page,
                PerPage = perPage,
                Pages = await clientService.Pages(perPage)
            };
            return response;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ClientResponse>> GetClient(string id)
        {
            Client client = await clientService.Get(id);
            return new ClientResponse(client);
        }

        [HttpPost]
        public async Task<ActionResult<ClientResponse>> CreateClient([FromBody]CreateClientRequest request)
        {
            var service = await clientService.Create(new Client
            {
                ClientId = Guid.NewGuid().ToString(),
                FirstName = request.FirstName,
                LastName = request.LastName
            });
            return new ClientResponse(service);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClient(string id)
        {
            await clientService.Delete(id);
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<ClientResponse>> UpdateClient(string id, UpdateClientRequest updateClientRequest)
        {
            if (id != updateClientRequest.ClientId)
            {
                return BadRequest();
            }

            Client client = await clientService.Update(updateClientRequest);
            return new ClientResponse(client);
        }
    }
}
