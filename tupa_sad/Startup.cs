using Employers.Api.Extensions;
using Clients.Api.Extensions;
using Equipments.Abstractions.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.IO;
using System.Linq;
using Requests.Api.Extensions;

namespace tupa_sad
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.InitializeEquipmentsServices(options =>
                options.UseNpgsql(Configuration.GetConnectionString("MyWebApiConection")));
            services.InitializeEmployersServices(options =>
                options.UseNpgsql(Configuration.GetConnectionString("MyWebApiConection")));
            services.InitializeClientsServices(options =>
                options.UseNpgsql(Configuration.GetConnectionString("MyWebApiConection")));


            services.InitializeRequestsServices(options =>
                options.UseNpgsql(Configuration.GetConnectionString("MyWebApiConection")));


            services.AddSwaggerGen(options =>
            {
                Directory.GetFiles(AppContext.BaseDirectory, "*.xml").ToList().ForEach(xmlFilePath => options.IncludeXmlComments(xmlFilePath));

                options.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
            }).AddSwaggerGenNewtonsoftSupport();

            //получаем зачёт
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


            app.UseSwagger();
            app.UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint("/swagger/v1/swagger.json", "API");
                x.RoutePrefix = string.Empty;
                x.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);
            });
        }
    }
}
